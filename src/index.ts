import { print } from '@functions/extra'

print('Hello world!')

import axios from 'axios'

axios
    .get('https://jsonplaceholder.typicode.com/posts')
    .then((res) => res.data)
    .then(console.log)
